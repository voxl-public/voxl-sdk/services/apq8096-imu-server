/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR bus[id]INESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#include <voxl_io.h>

#include "config_file.h"
#include "cal_file.h"
#include "imu_interface.h"
#include "mpu9250.h"
#include "icm20948.h"
#include "icm42688.h"

#ifndef unlikely
#define unlikely(x)	__builtin_expect (!!(x), 0)
#endif


// all MPU/ICM parts use spi mode 3, speed we may need to customize in the future
#define SPI_MODE	3
#define SPI_SPEED_INIT 1000000  // 1mbit, that's as slow as DSPAL will let us go

// colors for printing self-test results
#define COLOR_RED "\x1b[31m"
#define COLOR_GRN "\x1b[32m"
#define COLOR_RST "\x1b[0m"
#define LINE "=================================================================\n"


////////////////////////////////////////////////////////////////////////////////
// Local vars
////////////////////////////////////////////////////////////////////////////////
typedef enum imu_ic_t{
	IMU_IC_UNKNOWN	= 0,
	IMU_IC_MPU9250	= 1,
	IMU_IC_ICM20948	= 2,
	IMU_IC_ICM42688	= 3
} imu_ic_t;

// set on call to imu_detect
static imu_ic_t ic[N_IMUS];
static int board_id = BOARD_UNKNOWN;

// each imu gets its own shared memory for fifo reads
static uint8_t* rpc_shared_mem[N_IMUS];

// flag to indicate if the imu has been initialized and if the fifo is running
static int initialized[N_IMUS];
static int fifo_running[N_IMUS];
static int spi_running[N_IMUS];


static int imu_open_spi_bus(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}

	// allow this function to be called after init, just return
	if(spi_running[id]) return 0;

	if(voxl_spi_init(bus[id],SPI_MODE,SPI_SPEED_INIT)){
		fprintf(stderr, "ERROR: in imu_open_spi_bus[id]\n");
		return -1;
	}

	rpc_shared_mem[id] = voxl_rpc_shared_mem_alloc(FIFO_READ_BUF_LEN);
	if(rpc_shared_mem[id]==NULL){
		fprintf(stderr, "ERROR: failed to allocate RPC shared memory\n");
		return -1;
	}
	spi_running[id]=1;
	return 0;
}


static int imu_close_spi_bus(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}

	// allow this function to be called after cleanup or before init, just return
	if(!spi_running[id]) return 0;

	if(voxl_spi_close(bus[id])){
		fprintf(stderr, "ERROR: in imu_close_spi_bus[id]\n");
		return -1;
	}
	if(rpc_shared_mem[id]){
		voxl_rpc_shared_mem_free(rpc_shared_mem[id]);
		rpc_shared_mem[id]=0;
	}
	spi_running[id]=0;
	return 0;
}



int imu_self_test_print_results(int id, imu_self_test_result_t result)
{
	if(result.overall==TEST_PASS)	printf(COLOR_GRN LINE);
	else							printf(COLOR_RED LINE);

	if(result.overall==TEST_PASS)	printf(COLOR_GRN "RESULT FOR IMU%d on SPI BUS %d\n", id, bus[id]);
	else							printf(COLOR_RED "RESULT FOR IMU%d on SPI BUS %d\n", id, bus[id]);

	if(result.gyro[0]==TEST_PASS)	printf(COLOR_GRN "GYRO X: PASS\n");
	else							printf(COLOR_RED "GYRO X: FAIL\n");

	if(result.gyro[1]==TEST_PASS)	printf(COLOR_GRN "GYRO Y: PASS\n");
	else							printf(COLOR_RED "GYRO Y: FAIL\n");

	if(result.gyro[2]==TEST_PASS)	printf(COLOR_GRN "GYRO Z: PASS\n");
	else							printf(COLOR_RED "GYRO Z: FAIL\n");

	if(result.accl[0]==TEST_PASS)	printf(COLOR_GRN "ACCL X: PASS\n");
	else							printf(COLOR_RED "ACCL X: FAIL\n");

	if(result.accl[1]==TEST_PASS)	printf(COLOR_GRN "ACCL Y: PASS\n");
	else							printf(COLOR_RED "ACCL Y: FAIL\n");

	if(result.accl[2]==TEST_PASS)	printf(COLOR_GRN "ACCL Z: PASS\n");
	else							printf(COLOR_RED "ACCL Z: FAIL\n");

	if(result.overall==TEST_PASS)	printf(COLOR_GRN "OVERALL PASS\n");
	else if(result.overall==TEST_FAIL_COM){
		printf(COLOR_RED "OVERALL FAIL: COULD NOT COMMUNICATE WITH IMU\n");
	}
	else							printf(COLOR_RED "OVERALL FAIL\n");

	if(result.overall==TEST_PASS)	printf(COLOR_GRN LINE);
	else							printf(COLOR_RED LINE);

	// make sure color is back to normal before exiting
	printf(COLOR_RST);
	return 0;
}


int imu_print_data(int id, imu_data_t data)
{
	printf("imu%d: A:%6.2f %6.2f %6.2f G:%6.2f %6.2f %6.2f T: %6.2f ts: %llu\n",\
		id,
		(double)data.accl_ms2[0],(double)data.accl_ms2[1],(double)data.accl_ms2[2],\
		(double)data.gyro_rad[0],(double)data.gyro_rad[1],(double)data.gyro_rad[2],\
		(double)data.temp_c, data.timestamp_ns);
	return 0;
}



int imu_apply_calibration(int id, imu_data_t* data)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	// apply the base cal offsets
	for(int i=0;i<3;i++){
		data->gyro_rad[i] -= gyro_offset[id][i];
		data->accl_ms2[i] -= accl_offset[id][i];
	}

	// apply temp cal offset if it exists
	if(has_baseline_temp[id] && has_temp_cal[id]){
		float t  = data->temp_c;
		float tt = t*t;
		for(int i=0;i<3;i++){
			data->gyro_rad[i] -= corrected_gyro_drift_coeff[id][i][0] + \
								(corrected_gyro_drift_coeff[id][i][1]*t) +\
								(corrected_gyro_drift_coeff[id][i][2]*tt);
			data->accl_ms2[i] -= corrected_accl_drift_coeff[id][i][0] + \
								(corrected_accl_drift_coeff[id][i][1]*t) +\
								(corrected_accl_drift_coeff[id][i][2]*tt);
			/*
			printf(" gyro %d %f\n", i, corrected_gyro_drift_coeff[id][i][0] + \
								(corrected_gyro_drift_coeff[id][i][1]*t) +\
								(corrected_gyro_drift_coeff[id][i][2]*tt));
			printf(" accl %d %f\n", i, corrected_accl_drift_coeff[id][i][0] + \
								(corrected_accl_drift_coeff[id][i][1]*t) +\
								(corrected_accl_drift_coeff[id][i][2]*tt));
			*/
		}
	}

	// apply scale AFTER offset
	for(int i=0;i<3;i++){
		data->accl_ms2[i] /=  accl_scale[id][i];
	}

	return 0;
}


int imu_rotate_to_common_frame(int id, imu_data_t* data)
{
	float gtmp[3];
	float atmp[3];
	int i;

	// M0069 with 2 42688 imus, both on top of the board
	if(board_id == BOARD_M0069){
		switch(id){
			// imu on top by usb port
			case 0:
				data->gyro_rad[1] = -data->gyro_rad[1];
				data->accl_ms2[1] = -data->accl_ms2[1];
				data->gyro_rad[2] = -data->gyro_rad[2];
				data->accl_ms2[2] = -data->accl_ms2[2];
				break;

			// imu on top my MIPI camera connectors
			case 1:
				data->gyro_rad[0] = -data->gyro_rad[0];
				data->accl_ms2[0] = -data->accl_ms2[0];
				data->gyro_rad[2] = -data->gyro_rad[2];
				data->accl_ms2[2] = -data->accl_ms2[2];
				break;

			default:
				fprintf(stderr, "ERROR: in %s, imu must be 0 or 1\n", __FUNCTION__);
				return -1;
		}
	}
	// all previous VOXL boards
	else{
		switch(id){
			// imu on bottom by usb port
			case 0:
				data->gyro_rad[0] = -data->gyro_rad[0];
				data->gyro_rad[1] = -data->gyro_rad[1];
				data->accl_ms2[0] = -data->accl_ms2[0];
				data->accl_ms2[1] = -data->accl_ms2[1];
				break;

			// imu on top my MIPI camera connectors
			case 1:
				for(i=0;i<3;i++){
					gtmp[i]=data->gyro_rad[i];
					atmp[i]=data->accl_ms2[i];
				}
				data->gyro_rad[0] =  gtmp[1];
				data->gyro_rad[1] =  gtmp[0];
				data->gyro_rad[2] = -gtmp[2];
				data->accl_ms2[0] =  atmp[1];
				data->accl_ms2[1] =  atmp[0];
				data->accl_ms2[2] = -atmp[2];
				break;

			default:
				fprintf(stderr, "ERROR: in %s, imu must be 0 or 1\n", __FUNCTION__);
				return -1;
		}
	}
	return 0;
}


int imu_detect_board()
{
	if(imu_detect(0)){
		fprintf(stderr, "ERROR in %s detecting imu0\n", __FUNCTION__);
		return -1;
	}
	if(imu_detect(1)){
		fprintf(stderr, "ERROR in %s detecting imu1\n", __FUNCTION__);
		return -1;
	}

	// now decide which board we are on based on IMUs
	if(ic[1]==IMU_IC_ICM42688){
		board_id = BOARD_M0069;
		printf("Detected board M0069\n");
	}
	else if(ic[0]==IMU_IC_ICM42688 && ic[1]==IMU_IC_ICM20948){
		board_id = BOARD_M0019;
		printf("Detected board M0019\n");
	}
	else if(ic[0]==IMU_IC_ICM20948 && ic[1]==IMU_IC_ICM20948){
		board_id = BOARD_M0006;
		printf("Detected board M0006\n");
	}
	else if(ic[0]==IMU_IC_MPU9250){
		board_id = BOARD_HA942;
		printf("Detected board HA942\n");
	}
	else{
		board_id = BOARD_UNKNOWN;
		fprintf(stderr, "WARNING unknown VOXL board configuration\n");
		fprintf(stderr, " detected IMU 0 and 1 have types %d & %d\n", ic[0], ic[1]);
	}
	return 0;
}


int imu_detect(int id)
{
	if(imu_open_spi_bus(id)) return -1;

	// check if imu has already been identified, no need to do it again
	if(ic[id]!=IMU_IC_UNKNOWN) return 0;

	// Check for ICM20948
	if(icm20948_detect(bus[id])==0){
		ic[id] = IMU_IC_ICM20948;
		printf("detected ICM20948 on spi bus[id] %d\n", bus[id]);
		return 0;
	}

	// Check for ICM42688
	if(icm42688_detect(bus[id])==0){
		ic[id] = IMU_IC_ICM42688;
		printf("detected ICM42688 on spi bus[id] %d\n", bus[id]);
		return 0;
	}

	// Check for MPU9250
	if(mpu9250_detect(bus[id])==0){
		ic[id] = IMU_IC_MPU9250;
		printf("detected MPU9250 on spi bus[id] %d\n", bus[id]);
		return 0;
	}

	fprintf(stderr, "ERROR: imu_detect failed to find a valid part\n");
	return -1;
}


int imu_init(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}

	// already initialized, nothing to do
	if(initialized[id]) return 0;

	if(imu_open_spi_bus(id)) return -1;
	if(imu_detect(id)) return -1;

	switch(ic[id]){
	case IMU_IC_MPU9250:
		if(mpu9250_init(bus[id], imu_sample_rate_hz[id], imu_lp_cutoff_freq_hz[id])) return -1;
		break;
	case IMU_IC_ICM20948:
		if(icm20948_init(bus[id], imu_sample_rate_hz[id], imu_lp_cutoff_freq_hz[id])) return -1;
		break;
	case IMU_IC_ICM42688:
		if(icm42688_init(bus[id], imu_sample_rate_hz[id], imu_lp_cutoff_freq_hz[id])) return -1;
		break;
	default:
		fprintf(stderr, "ERROR: in imu_init() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	initialized[id]=1;	// all initialized now
	fifo_running[id]=0;	// but we haven't started the fifo yet!!!
	return 0;
}


int imu_close(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	if(!initialized[id]) return 0;

	// slow spi speed down for register config
	voxl_spi_set_freq(bus[id], SPI_SPEED_INIT);

	switch(ic[id]){
	case IMU_IC_MPU9250:
		if(mpu9250_close(bus[id])) return -1;
		break;
	case IMU_IC_ICM20948:
		if(icm20948_close(bus[id])) return -1;
		break;
	case IMU_IC_ICM42688:
		if(icm42688_close(bus[id])) return -1;
		break;
	default:
		fprintf(stderr, "ERROR: in imu_close() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	initialized[id]=0;
	fifo_running[id]=0;

	if(imu_close_spi_bus(id)) return -1;

	return 0;
}



int imu_basic_read(int id, imu_data_t* data)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	if(unlikely(!initialized[id])){
		fprintf(stderr, "ERROR: tried to call %s without initializing IMU first\n", __FUNCTION__);
		return -1;
	}

	switch(ic[id]){
	case IMU_IC_MPU9250:
		return mpu9250_basic_read(bus[id], data);
	case IMU_IC_ICM20948:
		return icm20948_basic_read(bus[id], data);
	case IMU_IC_ICM42688:
		return icm42688_basic_read(bus[id], data);
	default:
		fprintf(stderr, "ERROR: in imu_basic_read() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	return 0;
}


int imu_fifo_reset(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	if(unlikely(!initialized[id])){
		fprintf(stderr, "ERROR: tried to call %s without initializing IMU first\n", __FUNCTION__);
		return -1;
	}

	// slow spi speed down for register config
	voxl_spi_set_freq(bus[id], SPI_SPEED_INIT);

	switch(ic[id]){
	case IMU_IC_MPU9250:
		if(mpu9250_fifo_reset(bus[id])) return -1;
		break;
	case IMU_IC_ICM20948:
		if(icm20948_fifo_reset(bus[id])) return -1;
		break;
	case IMU_IC_ICM42688:
		if(icm42688_fifo_reset(bus[id])) return -1;
		break;
	default:
		fprintf(stderr, "ERROR: in imu_fifo_reset() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	fifo_running[id]=1;
	return 0;
}


int imu_fifo_start(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	if(unlikely(!initialized[id])){
		fprintf(stderr, "ERROR: tried to call %s without initializing IMU first\n", __FUNCTION__);
		return -1;
	}

	// already running, nothing to do
	if(fifo_running[id]) return 0;

	// slow spi speed down for register config
	voxl_spi_set_freq(bus[id], SPI_SPEED_INIT);

	switch(ic[id]){
	case IMU_IC_MPU9250:
		if(mpu9250_fifo_start(bus[id])) return -1;
		break;
	case IMU_IC_ICM20948:
		if(icm20948_fifo_start(bus[id])) return -1;
		break;
	case IMU_IC_ICM42688:
		if(icm42688_fifo_start(bus[id])) return -1;
		break;
	default:
		fprintf(stderr, "ERROR: in imu_fifo_start() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	fifo_running[id]=1;
	printf("started fifo on imu%d\n", id);
	return 0;
}


int imu_fifo_stop(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	if(unlikely(!initialized[id])){
		fprintf(stderr, "ERROR: tried to call %s without initializing IMU first\n", __FUNCTION__);
		return -1;
	}
	if(!fifo_running[id]) return 0;

	// slow spi speed down for register config
	voxl_spi_set_freq(bus[id], SPI_SPEED_INIT);

	switch(ic[id]){
	case IMU_IC_MPU9250:
		if(mpu9250_fifo_stop(bus[id])) return -1;
		break;
	case IMU_IC_ICM20948:
		if(icm20948_fifo_stop(bus[id])) return -1;
		break;
	case IMU_IC_ICM42688:
		if(icm42688_fifo_stop(bus[id])) return -1;
		break;
	default:
		fprintf(stderr, "ERROR: in imu_fifo_stop() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	fifo_running[id]=0;
	return 0;
}

int imu_is_fifo_running(int id)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	return fifo_running[id];
}


int imu_fifo_read(int id, imu_data_t* data, int* packets)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}
	if(unlikely(!initialized[id])){
		fprintf(stderr, "ERROR: tried to call %s without initializing IMU first\n", __FUNCTION__);
		return -1;
	}
	if(!fifo_running[id]) return 0;

	int min_packets = imu_read_every_n_samples[id];
	if(min_packets<1) min_packets = 1;

	switch(ic[id]){
	case IMU_IC_MPU9250:
		return mpu9250_fifo_read(bus[id], min_packets, data, packets, rpc_shared_mem[id]);
	case IMU_IC_ICM20948:
		return icm20948_fifo_read(bus[id], min_packets, data, packets, rpc_shared_mem[id]);
	case IMU_IC_ICM42688:
		return icm42688_fifo_read(bus[id], min_packets, data, packets, rpc_shared_mem[id]);
	default:
		fprintf(stderr, "ERROR: in imu_fifo_read() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	return 0;
}


int imu_self_test(int id, imu_self_test_result_t* result)
{
	// sanity checks
	if(unlikely(id<0||id>MAX_IMU)){
		fprintf(stderr, "ERROR: in %s, id out of bounds\n", __FUNCTION__);
		return -1;
	}

	switch(ic[id]){
	case IMU_IC_MPU9250:
		return mpu9250_self_test(bus[id], result);
	case IMU_IC_ICM20948:
		return icm20948_self_test(bus[id], result);
	case IMU_IC_ICM42688:
		return icm42688_self_test(bus[id], result);
	default:
		fprintf(stderr, "ERROR: in imu_self_test() invalid ic part number\n");
		fprintf(stderr, "call imu_detect first!\n");
		return -1;
	}
	return 0;
}
